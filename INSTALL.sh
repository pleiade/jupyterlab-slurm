#!/bin/bash

name=jupyterlab
: ${mod_dir:=/cm/shared/dev/modules/generic/modulefiles}

# Find current version if possible
_version=$(module add build/conda && conda activate jupyterlab && jupyter lab --version)

# Set version, category, modulefiles directory unless provided
: ${version:=${_version:-default}}
: ${category:=editor}

# Installation directories
: ${app_dir:=${mod_dir%modulefiles}apps/${category}/${name}/${version}}
: ${mod_file:=${mod_dir}/${category}/${name}/${version}}

# Install scripts
mkdir -p "${app_dir}"/bin
chmod g+rwxs,o=rx "${app_dir}" "${app_dir}"/bin

install -g plafrim-dev -m ug=rwx,o=rx -t "${app_dir}"/bin \
  jupyterlab-start
install -g plafrim-dev -m ug=rwx,o=rx -t "${app_dir}" \
  jupyterlab.bash \
  jupyterlab-job.bash

# Install modulefile
mkdir -p $(dirname "${mod_file}")
cat > "${mod_file}" <<EOF
#%Module

set name        ${name}
set version     ${version}
set category	${category}
set prefix      ${mod_dir%modulefiles}apps/\${category}/\${name}/\${version}

module-whatis "Jupyterlab - Collaborative Jupyter notebook server"

if {![file exists \$prefix]} {
    puts stderr "\t[module-info name] Load Error: \$prefix does not exist"
    break
    exit 1
}

prepend-path PATH \${prefix}/bin
prepend-path JUPYTERLAB_HOME \${prefix}

EOF
chmod g+rwxs,o=rx $(dirname "${mod_file}")
chmod g+rw,o=r "${mod_file}"
