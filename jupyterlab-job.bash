#!/bin/bash

# Job to run jupyterlab on a node, using a dynamically allocated port.

# The actual server address, such as zonda21.plafrim.cluster:46123, is
# reported as a jobs Comment as soon as the server starts responding. 
# Find the address using `squeue -h -O JobId,Comment:0`.
# Users will need an ssh PortForward or DynamicForward to reach it.

#SBATCH -J jpylab
#SBATCH --time=2:00:00
#SBATCH --mem=30G

# You can set values when submitting the Slurm job by using
# `sbatch --export=JUPYTER_NOTEBOOK=/some/directory,JUPYTER_PORT=8888`.
: ${JUPYTERLAB_HOME:=$PWD}
: ${JUPYTER_NOTEBOOK:=/beegfs/$USER/jupyterlab}
: ${JUPYTER_PORT:=}

# Path to job script. Usually don't change this.
: ${JUPYTERLAB_SCRIPT:=${JUPYTERLAB_HOME}/jupyterlab.bash}

set | grep JUPYTER

# Run the job step, setting the Comment to the actual server address
# ==================================================================
PIPE=$(mktemp -d)/pipe
mknod $PIPE p
trap "rm -rf ${PIPE} ${PIPE%/*}" EXIT ERR

srun --jobid=${SLURM_JOBID} \
     ${JUPYTERLAB_SCRIPT} >> $PIPE &

# First line of step stdout will be the actual server address.
head -1 < $PIPE | xargs -L 1 -I _ scontrol update job ${SLURM_JOBID} Comment=_

wait
