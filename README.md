# Hosting Jupyter Lab on a Slurm node

## Quickstart

```sh
jupyterlab-start
```

Example
```sh
plafrim$ ./jupyterlab-start
http://zonda18.plafrim.cluster:46316/lab
plafrim$ squeue -O JobId:.10,Partition:.10,Name:.9,UserName:.9,StateCompact:.3,TimeUsed:.11,NumNodes:.7,Comment:.42
     JOBID PARTITION     NAME     USER ST       TIME  NODES                                   COMMENT
   1366925   routage   jpylab  sherman  R      17:30      1  http://zonda19.plafrim.cluster:46316/labb
```

Firefox must be configured to use a SOCKS proxy for the `*.plafrim.cluster` domain. The following `proxy.pac` file can be used for automatic configuration:

```javascript
function FindProxyForURL(url, host){
    if ( dnsDomainIs(host,"plafrim.cluster") ) {
        return "SOCKS5 127.0.0.1:8123" ;
    }
}
```

## One-step session with SOCKS proxy

macOS:
```sh
open -a Firefox $(ssh -f -D 8123 plafrim srun jupyterlab.bash 2>/dev/null | head -1)
```

Linux:
```sh
firefox $(ssh -f -D 8123 plafrim srun jupyterlab.bash 2>/dev/null | head -1)
```
