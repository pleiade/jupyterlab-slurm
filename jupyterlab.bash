#!/bin/bash

# Step to run jupyterlab on a node, using a dynamically allocated port.

# The actual server address, such as zonda21.plafrim.cluster:46123, is
# reported on stdout as soon as the server starts responding. Users will
# need an ssh PortForward or DynamicForward to reach it.

# If you configure firefox to use a SOCKS proxy on port 8123 for Plafrim, then
# you can run this step directly to keep an ssh proxy open: to open a tunnel,
# wait for jupyterlab to start, and point firefox at the server, do
# `firefox $(ssh -f -D 8123 plafrim srun jupyterlab.bash 2>/dev/null | head -1)`

#SBATCH -J jpylab_s
#SBATCH --time=2:00:00
#SBATCH --mem=30G

# Jupyter lab is maintained in a Conda environment built with
# `conda install -c conda-forge/label/jupyterlab_rc jupyterlab`
module add build/conda
conda activate jupyterlab

# You can set values when submitting the Slurm job by using
# `srun --export=JUPYTER_NOTEBOOK=/some/directory,JUPYTER_PORT=8888`.
: ${JUPYTER_NOTEBOOK:=/beegfs/$USER/jupyterlab}
: ${JUPYTER_PORT:="$(ephemeral-port-reserve)"}

# Ensure directory for notebooks
mkdir -p ${JUPYTER_NOTEBOOK}

# Ensure user's global server config is initialized.
# You can manually add a server password by using
# `jupyter server password`.
[ -s $HOME/.jupyter/jupyter_server_config.py ] || jupyter server --generate-config

# Watch for the server in the background, report URL when it responds
URL=http://$(hostname):${JUPYTER_PORT}/lab
PROBE="--connect-timeout 5 --noproxy \* --silent --head --output /dev/null"
( until curl $URL $PROBE; do sleep 5; done; echo $URL ) &

# Run the server, all logs to stderr
# ==================================
jupyter lab --no-browser \
            --collaborative \
            --ip "*" \
            --port ${JUPYTER_PORT} \
            --notebook-dir ${JUPYTER_NOTEBOOK} \
            1>&2
